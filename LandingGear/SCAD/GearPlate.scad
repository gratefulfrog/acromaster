$fn=100;

//DXFFileName = "../DXF/GearSupport 01.dxf";
DXFFileName = "../DXF/GearSupport 02.dxf";
plateLayerName = "Plate";
gearHolesLayerName = "GearHoles";
vertical15mmLayerName = "Vertical15mm";
vertical5mmLayerNmae = "Vertical5mm";
vertical4mmLayerName = "Vertical4mm";

epsilon = 0.5;
plateZ = 3;
fifteen = 15;
four = 4;
five = 5;
extruderDia = 0.5;

module impExt(file,lay,z){
  linear_extrude(z)
    import(file,layer=lay);
}

module sacrificialTower(){
  x= extruderDia*8;
  translate([-0.5*x,-10,0])
    cube([x,x,plateZ+fifteen]);
}
//sacrificialTower();

module all(){
  difference(){
    union(){
      impExt(DXFFileName,plateLayerName,plateZ);
      impExt(DXFFileName,vertical15mmLayerName,fifteen+plateZ);
      impExt(DXFFileName,vertical5mmLayerNmae,five+plateZ);
      impExt(DXFFileName,vertical4mmLayerName,four+plateZ);
    }
    translate([0,0,-0.5*epsilon])
      impExt(DXFFileName,gearHolesLayerName,epsilon+plateZ);
  }
  //sacrificialTower();
}
all();

module testR(){
  rotate_extrude()
    import(DXFFileName, layer="test");
}
module doTestR(){
  translate([26,4,plateZ])
    testR();
  translate([-26,4,plateZ])
    testR();
}
doTestR();
linear_extrude(plateZ)
  projection()
    doTestR();


module testL(){
  translate([0,0,plateZ])
    rotate([0,-90,0])
      linear_extrude(18)
        translate([-2,0,0])
          import(DXFFileName, layer="test");
}
translate([9,-15.4306,0])
  testL();
translate([9,28.7224,0])
  testL();

translate([9,-18.4306,0])
  mirror([0,1,0])
    testL();

translate([9,25.7224,0])
  mirror([0,1,0])
    testL();
